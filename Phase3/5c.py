import matplotlib.pyplot as plt
import numpy as np
from matplotlib import style
import pandas as pd
import math
import tfidf
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn.metrics.pairwise import euclidean_distances
style.use('ggplot')

class Support_Vector_Machine:
    # train
    def fit(self, data):
        self.data = data

        # { ||w||: [w, b]}
        opt_dict = {}

        transforms = [[1, 1],
                      [1, -1],
                      [-1, 1],
                      [-1, -1]]
        # transforms = [[1, 1, 1, 1],
        #               [1, -1, 1, 1],
        #               [-1, 1, 1, 1],
        #               [-1, -1, 1, 1],
        #               [-1, 1, -1, 1],
        #               [1, -1, -1, 1],
        #               [-1, 1, -1, 1],
        #               [-1, -1, -1, 1],
        #               [1, 1, 1, -1],
        #               [1, -1, 1, -1],
        #               [-1, 1, 1, -1],
        #               [-1, -1, 1, -1],
        #               [-1, 1, -1, -1],
        #               [1, -1, -1, -1],
        #               [-1, 1, -1, -1],
        #               [-1, -1, -1, -1],
        #               [1, 1, 1, 1],
        #               [1, -1, 1, 1],
        #               [-1, 1, 1, 1],
        #               [-1, -1, 1, 1],
        #               [-1, 1, -1, 1],
        #               [1, -1, -1, 1],
        #               [-1, 1, -1, 1],
        #               [-1, -1, -1, 1]]
        # # transforms = [[1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        #               [-1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        #               [-1, -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        #               [1, -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]]

        all_data = []
        for yi in self.data:
            for featureset in self.data[yi]:
                for feature in featureset:
                    all_data.append(feature)

        self.max_feature_value = max(all_data)
        self.min_feature_value = min(all_data)
        all_data = None

        step_sizes = [self.max_feature_value * 0.1,
                      self.max_feature_value * 0.01,
                      self.max_feature_value * 0.001]

        # extremely expensive
        b_range_multiple = 2

        b_multiple = 5

        latest_optimum = self.max_feature_value * 10

        for step in step_sizes:
            w = np.array([latest_optimum, latest_optimum])
            # w = np.array([latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum, latest_optimum])
            optimized = False
            while not optimized:
                for b in np.arange(-1*(self.max_feature_value*b_range_multiple),
                                   self.max_feature_value*b_range_multiple,
                                   step*b_multiple):
                    for transformation in transforms:
                        w_t = w * transformation
                        #print w_t
                        found_option = True
                        # print found_option
                        # yi(xi.w+b) >=1
                        for i in self.data:
                            for xi in self.data[i]:
                                yi = i

                                # if not yi * ((np.dot(w_t, (xi*xi)))+ np.dot(w_t, xi) + b) >= 1:
                                x = not yi * (np.dot(w_t, xi) + b) >= 1
                                #print x, yi, yi * (np.dot(w_t, xi) + b)
                                if x:
                                    found_option = False

                        if found_option:
                            print "Here"
                            opt_dict[np.linalg.norm(w_t)] = [w_t, b]
                if w[0] < 0:
                    optimized = True
                else:
                    #print w
                    w = w - step
            if len(opt_dict) == 0:
                print "Data is not linearlly separable"
                return False
            else:
                norms = sorted([n for n in opt_dict])
                # ||w|| : [w, b]
                opt_choice = opt_dict[norms[0]]
                self.w = opt_choice[0]
                self.b = opt_choice[1]
                latest_optimum = opt_choice[0][0] + step * 2
        return True

    def predict(self, features):
        # sign( w.x + b)
        classification = np.sign(np.dot(np.array(features), self.w) + self.b)
        return classification

# data_dict = {-1: np.array([[1, 7],
#                            [2, 8],
#                            [3, 8]]),
#              1: np.array([[5, 1],
#                           [6, -1],
#                           [7, 3]])}

movieGenreDF = pd.read_csv("Data/smallmlmovies.csv")
movieTagDF = pd.read_csv("Data/mltags.csv")
movieActorDF = pd.read_csv("Data/movie-actor.csv")
genomeTags = pd.read_csv('Data/genome-tags.csv')
actorInfo = pd.read_csv("Data/imdb-actor-info.csv")

actorInfo['actorid'] = actorInfo['id']
del actorInfo['id']

movieTag = pd.DataFrame(columns=['movieid', 'tagid'])
movieTag['movieid'] = movieTagDF['movieid']
movieTag['tag'] = movieTagDF['tagid']
movieTag = pd.merge(movieTag, movieGenreDF, on='movieid')
del movieTag['movieid']
del movieTag['year']
del movieTag['genres']

s = movieGenreDF["genres"].str.split('|', expand=True).stack()
i = s.index.get_level_values(0)
movieGenre = movieGenreDF.loc[i].copy()
movieGenre["genres"] = s.values

movieGenreDict = {k: list(v) for k, v in movieGenre.groupby('moviename')['genres']}
movieTagDict = {k: list(v) for k, v in movieTag.groupby('moviename')['tagid']}

movieList = movieGenreDF.movieid.unique()
movieList = np.asarray(movieList)
tagList = movieTagDF.tagid.unique()
tagList = np.asarray(tagList)

movieTagMatrix = np.zeros((movieList.shape[0], tagList.shape[0]))
for i in range(len(movieList)):
    output = tfidf.function(movieList[i])
    if output != "Nothing found":
        for j in range(len(tagList)):
            if output.has_key(tagList[j]):
                movieTagMatrix[i][j] = output.get(tagList[j])

# Reduce dimensions
n = 2
pca = decomposition.PCA(n_components=2)
tag_pca = pca.fit_transform(movieTagMatrix)
# for i in range(len(tag_pca)):
#     for j in range(len(tag_pca[0])):
#         tag_pca[i][j] += 2
similarity = euclidean_distances(movieTagMatrix, movieTagMatrix)
#dict1 = {'Action': [3366, 6057, 9443, 10059, 5044], 'Thriller': [3189, 5959, 6163, 9448]} # , 'Adventure': [3854, 4354, 4869]}
dict1 = {'Thriller':[8973], 'Comedy':[7852]}
labels = []
dict_svm = {}
for key, value in dict1.iteritems():
    labels.append(key)
label_dict = {}
classLabel = [1, -1]
for i in range(len(labels)):
    label_dict[labels[i]] = classLabel[i]
dict2 = {}
movieList = movieList.tolist()
# dict2[1] = np.array([[0.065, 0.0844]])
# dict2[-1] = np.array([[0.064, 0.0847]])
for key, value in dict1.iteritems():
    values = dict1[key]
    arr = []
    for j in range(len(values)):
        mIndex = movieList.index(values[j])
        v = tag_pca[mIndex]
        v = v.tolist()
        arr.append(v)
    dict2[label_dict[key]] = np.array(arr)
print dict2
print label_dict
dict2[1] = np.array([[0.065, 0.0844]])
dict2[-1] = np.array([[0.064, 0.0847]])
d = np.array([[-0.06587826, -0.0844991], [-0.06455958, -0.08479269]])
#d1 = np.array([[-0.06455958, -0.08479269]])
# d1 = np.array([[-0.09725366, -0.10823822],
#        [-0.06183246, -0.08013594],
#        [-0.06416581, -0.0851558 ]])
# # x,y = d1.T
# # plt.scatter(x,y)
# # plt.show()
# #
# x,y = d.T
# plt.scatter(x,y)
# plt.show()
svm = Support_Vector_Machine()
f = svm.fit(data=dict2)
if f:
    predict_us = tag_pca
    for p in predict_us:
        c = svm.predict(p)
        print p, c
