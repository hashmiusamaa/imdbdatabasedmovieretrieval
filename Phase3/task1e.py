import t1a4e as apca
import task1d_for_1e as ppr
import task1bfor1e as lda
import task1c2c as cp
import dataframes as df
userID = cp.userid


def task1e():
    # movieid, similarity
    pca_df = apca.get_similar_movie(apca.__get_movies_watch_by_user__(int(userID)), int(userID))
    max_sim = max(pca_df['similarity'])
    min_sim = min(pca_df['similarity'])

    pca_df['normsim'] = ((max_sim - pca_df['similarity'])/(max_sim - min_sim + 1))
    del pca_df['moviename']
    del pca_df['rating']
    del pca_df['year']
    del pca_df['similarity']
    # print pca_df

    # movieid, pagerank
    ppr_df = ppr.get_output(userID)
    max_pr = max(ppr_df['pagerank'])
    min_pr = min(ppr_df['pagerank'])
    # already is in the format of bigger value better movie recommendation
    ppr_df['normpr'] = (max_pr - ppr_df['pagerank'])/(max_pr-min_pr+1)
    del ppr_df['name']
    del ppr_df['pagerank']
    # print ppr_df

    # movie, freq -> name here not id
    cp_df = cp.task1cFunc(userID)
    max_freq = max(cp_df['freq'])
    min_freq = min(cp_df['freq'])
    # already in the format bigger the value better the movie
    cp_df['normfreq'] = (max_freq - cp_df['freq'])/(max_freq - min_freq + 1)
    cp_final = df.movies_data.merge(cp_df, left_on='moviename', right_on='movie')
    del cp_final['moviename']
    del cp_final['year']
    del cp_final['genre']
    del cp_final['movie']
    del cp_final['freq']
    cp_final = cp_final.groupby(['movieid','normfreq']).count().reset_index().sort_values('normfreq', ascending=False)
    # print cp_final




    # movie, dist -> id
    lda_df = lda.get_lda_recommendations(userID, 'tf')
    max_dist = max(lda_df['dist'])
    min_dist = min(lda_df['dist'])
    # smaller the distance, better the movie, so converted it into
    # bigger value better distance
    lda_df['normdist'] = 1 - ((max_dist - lda_df['dist'])/(max_dist - min_dist + 1))
    lda_df['movieid'] = lda_df['movie']
    del lda_df['dist']
    del lda_df['movie']

    # print lda_df

    # print len(cp_final)
    # print len(pca_df)
    # print len(lda_df)
    # print len(ppr_df)

    final = ppr_df.merge(pca_df, how='left', on='movieid')
    final = final.merge(lda_df, how='left', on='movieid')
    final = final.merge(cp_final, how='left', on='movieid')
    np = min(ppr_df['normpr'])
    nd = min(lda_df['normdist'])
    nf = min(cp_final['normfreq'])
    ns = min(pca_df['normsim'])
    final[['normsim']] = final[['normsim']].fillna(value=ns)
    final[['normpr']] = final[['normpr']].fillna(value=np)
    final[['normfreq']] = final[['normfreq']].fillna(value=nf)
    final[['normdist']] = final[['normdist']].fillna(value=nd)
    final['rank'] = final['normsim'] + final['normdist'] + final['normfreq'] + final['normpr']
    final = final.sort_values('rank', ascending=False)
    print final.head(5)
    while(True):
        l = raw_input('Please enter list of movieIDs you disliked, comma separated')
        l = l.split(',')
        badnpr = -1
        badnsim = -1
        badndist = -1
        badnfreq = -1
        for movie in l:
            badnpr = final.loc[final['movieid']== int(movie)].iloc[0]['normpr']
            badnsim = final.loc[final['movieid'] == int(movie)].iloc[0]['normsim']
            badndist = final.loc[final['movieid'] == int(movie)].iloc[0]['normdist']
            badnfreq = final.loc[final['movieid'] == int(movie)].iloc[0]['normfreq']
            print badnpr, badndist, badnfreq, badnsim
            final['normsim'] = final['normsim'].apply(lambda x: rep(x, badnsim))
            final['normdist'] = final['normdist'].apply(lambda x: rep(x, badndist))
            final['normfreq'] = final['normfreq'].apply(lambda x: rep(x, badnfreq))
            final['normpr'] = final['normpr'].apply(lambda x: rep(x, badnpr))

        final['rank'] = final['normsim'] + final['normdist'] + final['normfreq'] + final['normpr']
        final = final.sort_values('rank', ascending=False)
        print final.head(5)


def rep(x, badnsim):
    if (badnsim - (0.02 * badnsim)) < x < (badnsim + (0.02 * badnsim)):
        return x * 0.6
    else:
        return x

task1e()