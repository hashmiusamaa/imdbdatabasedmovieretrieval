Command: python task1b.py tag tf 9

Output: Movies Watched By User
-----------------------
Grease
Gone with the Wind
Brick
Creature Comforts
Eternal Sunshine of the Spotless Mind
Traffic
American Beauty
Big
Gattaca
Almost Famous
Memento
Ed Wood
Happy Gilmore
This Is Spinal Tap
Kiss Kiss Bang Bang
Grosse Pointe Blank
Office Space
Annie Hall
Superman
Movies Recommended:
------------------ 
[{'distance': 7.161870607674814e-08, 'id': 6012, 'name': 'Paradise'}, {'distance': 2.4955691180217343e-07, 'id': 6785, 'name': 'Shoah'}, {'distance': 1.3543365917634276e-07, 'id': 6134, 'name': 'Born Yesterday'}, {'distance': 5.4393156645460294e-12, 'id': 3394, 'name': 'Lucas'}, {'distance': 7.822924503742001e-07, 'id': 538, 'name': 'Blade Runner'}]

Please enter the index of movies you dont like separated by space